json.array!(@articulos) do |articulo|
  json.extract! articulo, :id, :titulo, :cuerpo, :fecha
  json.url articulo_url(articulo, format: :json)
end
