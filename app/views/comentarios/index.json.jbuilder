json.array!(@comentarios) do |comentario|
  json.extract! comentario, :id, :autor, :cuerpo, :fecha
  json.url comentario_url(comentario, format: :json)
end
